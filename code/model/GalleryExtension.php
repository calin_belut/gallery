<?php
class GalleryExtension extends DataExtension {

    public static $has_many = array("GalleryImages" => "GalleryImage");
    public static $db = array();

    public function updateCMSFields(FieldList $fields) {

        $silderconfig = GridFieldConfig_RecordEditor::create();
        $silderconfig -> addComponent(new GridFieldSortableRows("Sort"));
        $silderconfig -> addComponent(new GridFieldBulkUpload());
        $silderconfig -> addComponent(new GridFieldBulkManager());
        $images = $this -> owner -> GalleryImages();
        $gridfieldslider = new GridField("Gallery", "Gallery", $images, $silderconfig);
        $fields -> addFieldToTab('Root.Gallery', $gridfieldslider);

    }

    public function onBeforeWrite() {

    }

}
