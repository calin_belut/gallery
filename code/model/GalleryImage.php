<?php
class GalleryImage extends DataObject {

    public static $db = array(
        'Sort' => 'Int',
        'Title' => 'Varchar(300)',
        "Content" => "HTMLText",
        "Link" => "Varchar(300)",
        "OpenInNewTab" => "Boolean",
    );

    // One-to-one relationship with gallery page
    public static $has_one = array(
        'Image' => 'Image',
        'Owner' => 'SiteTree'
    );

    // tidy up the CMS by not showing these fields
    public function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields -> removeFieldFromTab("Root.Main", "OwnerID");
        $fields -> removeFieldFromTab("Root.Main", "Sort");
        if ($this -> Owner() -> ClassName == "ServiceBox") {
            $fields -> removeFieldFromTab("Root.Main", "Content");
            $fields -> removeFieldFromTab("Root.Main", "Link");
            $fields -> removeFieldFromTab("Root.Main", "OpenInNewTab");
        }

        return $fields;
    }

    // Tell the datagrid what fields to show in the table
    public static $summary_fields = array(
        'Title' => 'Title',
        'Thumbnail' => 'Thumbnail'
    );

    // this function creates the thumnail for the summary fields to use
    public function getThumbnail() {

        return $this -> Image() -> CMSThumbnail();
    }

}
